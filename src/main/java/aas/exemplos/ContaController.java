package aas.exemplos;

import javax.websocket.server.PathParam;

import aas.exemplos.respostas.NomeApp;
import aas.exemplos.respostas.TokenApp;
import aas.exemplos.respostas.UserIdApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.util.Optional;

@RestController
public class ContaController {
    
    @Autowired
    ContaRepository contaRepository;
    
    private static final BigInteger APP_ID = new BigInteger("708099943345144");
    private static final String APP_SECRET = "7dab25f22bc366186ac27a06b25a4754";
    private static final String APP_TOKEN = "708099943345144|gQkvv428tpbaQ4PZl2WSWSI9qn4";
    
	@GetMapping("/conta/{id}")
	public ResponseEntity<?> recuperaConta(@PathVariable("id") Long id) {
        Optional<Conta> conta = contaRepository.findById(id);
        
        // Endpoint 01:
        // Verifica se existe alguma Conta com o id igual ao id recebido como parâmetro
        // Se o identificador foi encontrado
        //     Retorna um JSON contendo os dados da conta;
        //     O status da resposta é um http 200;
        if(conta.isPresent())
            return new ResponseEntity<>(conta.get(), HttpStatus.OK);
        
        
        // Caso o identificador não seja encontrado
        //     O status da resposta é um http redirect 307;
        //     Adiciona na resposta um cabeçalho "Location", cujo valor é a página de login do facebook com os devidos parâmetros:
        //         cliente_id: o client_id da aplicação;
        //         redirect_uri: indica que o facebook deverá redirecionar a aplicação para o Endpoint 02, utilizando o id de entrada;
        //         state: uma string utilizada para garantir do remetente da mensagem.
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Location", "https://www.facebook.com/v3.3/dialog/oauth?client_id="+APP_ID
                +"&redirect_uri=http://localhost:8082/conta/cadastro/"+id+"&state=create");
        
        return new ResponseEntity<>(null,responseHeaders,HttpStatus.TEMPORARY_REDIRECT);
	}
	
	@GetMapping("/conta/cadastro/{id}")
	public ResponseEntity<?> cadastrarConta(@PathVariable("id") Long id, @PathParam("code") String code, @PathParam("state") String state) {
		
		// Endpoint 02:
		// Realiza uma série de 3 chamadas para o servidor do facebook
		//     1) Envia o code para obter o token de acesso;
        RestTemplate restTemplate = new RestTemplate();
        TokenApp responseEntity = restTemplate.getForObject("https://graph.facebook.com/v3.3/oauth/access_token?client_id="+APP_ID+"&redirect_uri=" +
                        "http://localhost:8082/conta/cadastro/"+id+"&state="+state+"&client_secret="+ APP_SECRET +"&code="+code, TokenApp.class);
        
        //     2) Envia o token para ser auditado e obter id do usuário;
        UserIdApp dataAcess = restTemplate.getForObject("https://graph.facebook.com/debug_token?input_token="+responseEntity.getAccess_token()
                +"&access_token="+ APP_TOKEN, UserIdApp.class);
        
        //     3) Obtem os dados da conta.
        NomeApp nomeApp = restTemplate.getForObject("https://graph.facebook.com/"+dataAcess.getData().getUser_id()
                +"/?access_token="+responseEntity.getAccess_token(), NomeApp.class);
		
		// Salva os dados do usuário recebidos na terceira requisição
        contaRepository.save(new Conta(id, nomeApp));
        
		// Retorna um redirecionamento para o usuário:
		//     O status da resposta é um http redirect 307;
		//     Adiciona na resposta um cabeçalho "Location", cujo valor é o Endpoint 01 com o identificador do usuário
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Location", "http://localhost:8082/conta/"+id);
        return new ResponseEntity<>(null,responseHeaders,HttpStatus.TEMPORARY_REDIRECT);
	}
}
