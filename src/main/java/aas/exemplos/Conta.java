package aas.exemplos;

import aas.exemplos.respostas.NomeApp;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Conta {
	
    @Id
	private Long id;
	
	private String facebookId;
	
	private String nome;
    
    public Conta() {
    }
    
    public Conta(Long id, NomeApp nomeApp) {
        this.id = id;
        this.facebookId = nomeApp.getId();
        this.nome = nomeApp.getName();
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
