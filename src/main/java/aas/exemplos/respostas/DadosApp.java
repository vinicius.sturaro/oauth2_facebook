package aas.exemplos.respostas;

public class DadosApp {
    
    private String user_id;
    
    public DadosApp() {
    }
    
    public String getUser_id() {
        return user_id;
    }
    
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
