package aas.exemplos.respostas;

public class UserIdApp {
    
    private DadosApp data;
    
    public UserIdApp() {
    }
    
    public DadosApp getData() {
        return data;
    }
    
    public void setData(DadosApp data) {
        this.data = data;
    }
}
