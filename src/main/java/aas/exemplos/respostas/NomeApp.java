package aas.exemplos.respostas;

public class NomeApp {
    
    private String name;
    private String id;
    
    public NomeApp() {
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
}
